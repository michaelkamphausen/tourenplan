import Vapor

final class LegalViewController {
    
    func setup(router: Router) throws {
        router.get(Route.aboutUs, use: aboutUs)
        router.get(Route.privacyPolicy, use: privacyPolicy)
        router.get(Route.openSource, use: openSource)
    }
    
    func aboutUs(req: Request) throws -> Future<View> {
        let context = contextFromEnvironment(keys: ["address", "representative", "contact"])
        
        return try req.view().render("aboutUs", context)
    }
    
    func privacyPolicy(req: Request) throws -> Future<View> {
        let context = contextFromEnvironment(keys: ["address", "contact"])
        
        return try req.view().render("privacyPolicy", context)
    }
    
    func openSource(req: Request) throws -> Future<View> {
        let context = contextFromEnvironment(keys: [])
        
        return try req.view().render("openSource", context)
    }
    
    private func contextFromEnvironment(keys: [String]) -> [String: String?] {
        return Dictionary(uniqueKeysWithValues: keys.map({ ($0, Environment.get($0.uppercased())) }))
    }

}
