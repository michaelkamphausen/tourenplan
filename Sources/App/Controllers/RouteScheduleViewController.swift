import Vapor

final class RouteScheduleViewController {
    
    private static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    func setup(router: Router, restricted: Router) throws {
        restricted.get(Route.start, use: renderNavigation)
        restricted.get(Route.routeSchedule, use: renderNavigation)
        router.get(Route.routeSchedule, String.parameter, use: renderSchedule)
    }
    
    func renderNavigation(req: Request) throws -> Future<View> {
        let dmrzUrl = req.dmrzUrl ?? DMRZApiRequest.baseUrl.absoluteString
        let context = [
            "dmrzUrl": dmrzUrl,
            "date": RouteScheduleViewController.dateFormatter.string(from: Date())
        ]
        req.absence = nil
        
        return try req.view().render("routeScheduleNavigation", context)
    }
    
    func renderSchedule(req: Request) throws -> Future<Response> {
        guard let sessionID = req.dmrzSessionId else {
            return req.future(req.response(http: HTTPResponse(status: .forbidden)))
        }
        
        let dateString = (try? req.parameters.next(String.self)).value(or: "-")
        let date = RouteScheduleViewController.dateFormatter.date(from: dateString)
        
        let future: Future<RouteSchedule>
        if let date = date {
            future = try DMRZApiRequest(request: req, sessionId: sessionID).getRouteSchedule(date)
        } else {
            future = req.future(error: CustomError.invalidUrl)
        }
        
        return future
            .flatMap(to: Response.self, { routeSchedule in
                let context = RouteScheduleContext(routeSchedule: routeSchedule, date: date, error: nil)
                
                return try req.view().render("routeSchedule", context).encode(for: req)
            }).catchFlatMap({ error in
                let url = DMRZApiRequest.baseUrl.addQueryItems([URLQueryItem(name: "sid", value: req.dmrzSessionId)])
                let externalLinkAttributes = "href=\"\(url)\" target=\"_blank\" rel=\"noopener noreferrer nofollow\""
                let errorMessage: String
                switch error {
                case CustomError.loginRequired:
                    try? req.unauthenticateDmrzSession()
                    return req.future(req.response(http: HTTPResponse(status: .forbidden)))
                case CustomError.tanInputRequired:
                    return req.future(req.response(http: HTTPResponse(status: .forbidden)))
                case CustomError.invalidUrl:
                    errorMessage = "Ungültiges Datum."
                case CustomError.missingData:
                    errorMessage = "Für diesen Tag sind keine Touren geplant."
                case CustomError.cancelledRequest:
                    errorMessage = "Die Abfrage der Tourendaten wurde abgebrochen. Bitte erneut versuchen."
                case CustomError.unexpectedResponse:
                    errorMessage = "DMRZ hat eine unerwartete Antwort gesendet. Möglicherweise hat sich die Schnittstelle geändert und diese Software muss angepasst werden."
                case CustomError.tanListDownloadRequired:
                    errorMessage = "DMRZ hat eine neue TAN-Liste für Sie. Bitte <a \(externalLinkAttributes)>laden Sie zuerst die neue TAN-Liste in DMRZ herunter</a> und bestätigen Sie den Empfang."
                case CustomError.tanSetupRequired:
                    errorMessage = "DMRZ möchte, dass Sie ein neues TAN-Verfahren für ihr Nutzerkonto einrichten. Bitte <a \(externalLinkAttributes)>folgen Sie den Anweisungen in DMRZ</a> und melden Sie sich danach beim Tourenplan neu an."
                case CustomError.unavailable:
                    errorMessage = "DMRZ ist momentan nicht erreichbar."
                default:
                    errorMessage = "Leider ist ein unbekannter Fehler beim Abfragen der Tourendaten aufgetreten. Bitte erneut versuchen und die Seite neu laden. <span class=\"invisible\">\(error.localizedDescription)</span>"
                }

                let context = RouteScheduleContext(routeSchedule: RouteSchedule(tourGroups: [], absences: nil), date: date, error: errorMessage)

                return try req.view().render("routeSchedule", context).encode(for: req)
            })
    }

}
