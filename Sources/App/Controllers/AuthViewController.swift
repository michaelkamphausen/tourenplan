import Vapor

final class AuthViewController {
    
    func setup(router: Router) throws {
        router.get(Route.login, use: renderAuth)
        router.get(Route.loginWithDMRZUsername, use: renderAuth)
        router.post(DMRZLoginRequest.self, at: Route.loginWithDMRZUsername, use: loginWithDMRZUsername)
        router.get(Route.loginWithDMRZTan, use: renderAuth)
        router.post(DMRZTanRequest.self, at: Route.loginWithDMRZTan, use: loginWithDMRZTan)
        router.get(Route.logout, use: logout)
        router.get(Route.keepAlive, use: keepAlive)
    }
    
    func loginWithDMRZUsername(req: Request, content: DMRZLoginRequest) throws -> Future<Response> {
        if let error = content.validationError() {
            return try self.renderAuth(req, context: AuthContext(authType: .login, error: error, login: content))
        }
        
        let dmrzUrl = DMRZApiRequest.baseUrl.absoluteString
        
        return try DMRZApiRequest(request: req, sessionId: "").login(username: content.username, password: content.password)
            .flatMap({ arguments in
                let ((dmrzSid, cookies), (authState, tanLabel)) = arguments
                try req.authenticateDmrzSession(sessionId: dmrzSid, url: dmrzUrl, authState: authState, tanLabel: tanLabel, cookies: cookies)
                
                if authState == .authenticated {
                    return req.future(req.redirect(to: .start))
                } else {
                    return try self.renderAuth(req, context: AuthContext(authType: .tan, tanLabel: req.dmrzTanLabel))
                }
            }).catchFlatMap({ error in
                if error as? CustomError == .tanInputRequired {
                    return try self.renderAuth(req, context: AuthContext(authType: .tan, tanLabel: req.dmrzTanLabel))
                }
                return try self.renderAuth(req, context: AuthContext(authType: .login, error: error, login: content))
            })
    }
    
    func loginWithDMRZTan(req: Request, content: DMRZTanRequest) throws -> Future<Response> {
        if let error = content.validationError() {
            return try self.renderAuth(req, context: AuthContext(authType: .tan, error: error, tanLabel: req.dmrzTanLabel))
        }
        
        guard let sessionId = req.dmrzSessionId else {
            return try renderAuth(req, context: AuthContext(authType: .login))
        }
        
        return try DMRZApiRequest(request: req, sessionId: sessionId).login(tan: content.tan, authState: req.dmrzAuthenticationState)
            .flatMap({ authState, tanLabel in
                try req.authenticateDmrzSession(authState: authState, tanLabel: tanLabel)
                
                if [Request.AuthenticationState.requiresTan, .requiresOTP].contains(authState) {
                    return try self.renderAuth(req, context: AuthContext(authType: .tan, error: CustomError.tanIncorrect, tanLabel: tanLabel))
                } else {
                    return req.future(req.redirect(to: .start))
                }
            }).catchFlatMap({ error in
                var error = error
                
                if (error as? CustomError) == .tanInputRequired {
                    error = CustomError.tanIncorrect
                }
                
                return try self.renderAuth(req, context: AuthContext(authType: .tan, error: error, tanLabel: req.dmrzTanLabel))
            })
    }
    
    func loginWithDMRZSession(req: Request, content: DMRZSessionRequest) throws -> Future<Response> {
        guard let dmrzUrl = URL(string: content.dmrzUrl),
            let urlComponents = URLComponents(url: dmrzUrl, resolvingAgainstBaseURL: false),
            dmrzUrl.host == DMRZApiRequest.baseUrl.host else {
                return try renderAuth(req, context: AuthContext(authType: .session, error: CustomError.invalidUrl))
        }
        
        guard let dmrzSid = urlComponents.queryItems?.filter({ $0.name == "sid" }).first?.value,
            !dmrzSid.isEmpty else {
                return try renderAuth(req, context: AuthContext(authType: .session, error: CustomError.missingSessionId))
        }
        
        return try DMRZApiRequest(request: req, sessionId: dmrzSid).loginWithSessionId()
            .map({ _ in
                try req.authenticateDmrzSession(sessionId: dmrzSid, url: dmrzUrl.absoluteString, authState: .authenticated)
                return req.redirect(to: .start)
            }).catchFlatMap({ error in
                return try self.renderAuth(req, context: AuthContext(authType: .session, dmrzUrl: dmrzUrl, error: error))
            })
    }
    
    func logout(req: Request) throws -> Response {
        try req.unauthenticateDmrzSession()
        return req.redirect(to: .start)
    }
    
    func keepAlive(req: Request) throws -> Future<Response> {
        guard let sessionId = req.dmrzSessionId else {
            return req.future(req.response(http: HTTPResponse(status: .forbidden)))
        }
        
        return try DMRZApiRequest(request: req, sessionId: sessionId).keepAlive()
            .flatMap({ _ in
                return req.future(req.response(http: HTTPResponse(status: .ok)))
            }).catchFlatMap({ error in
                if (error as? CustomError) == .loginRequired {
                    try? req.unauthenticateDmrzSession()
                    return req.future(req.response(http: HTTPResponse(status: .forbidden)))
                }
                
                return req.future(req.response(http: HTTPResponse(status: .ok)))
            })
    }
    
    private func renderAuth(_ req: Request) throws -> Future<Response> {
        let authType: AuthContext.AuthType
        switch req.dmrzAuthenticationState {
        case .requiresTan, .requiresOTP:
            authType = .tan
        default:
            authType = .login
        }
        
        return try renderAuth(req, context: AuthContext(authType: authType, tanLabel: req.dmrzTanLabel))
    }
    
    private func renderAuth(_ req: Request, context: AuthContext) throws -> Future<Response> {
        guard try !req.hasAuthenticatedDmrzSession() else {
            return req.future(req.redirect(to: .start))
        }
        
        return try req.view().render("auth", context).encode(for: req)
    }
    
}
