import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let redirectMiddleware = RedirectMiddleware(Route.login.path)
    let restricted = router.grouped(redirectMiddleware)
    
    let routeScheduleViewController = RouteScheduleViewController()
    try routeScheduleViewController.setup(router: router, restricted: restricted)
    
    let authViewController = AuthViewController()
    try authViewController.setup(router: router)
    
    let legalViewController = LegalViewController()
    try legalViewController.setup(router: router)
}


