import Vapor

/// Basic middleware to redirect unauthenticated requests to the supplied path
public struct RedirectMiddleware: Middleware {
    /// The path to redirect to
    let path: String
    
    /// Initialise the `RedirectMiddleware`
    ///
    /// - parameters:
    ///    - path: The path to redirect to if the request is not authenticated
    public init(_ path: String) {
        self.path = path
    }
    
    /// See Middleware.respond
    public func respond(to req: Request, chainingTo next: Responder) throws -> Future<Response> {
        if try req.hasAuthenticatedDmrzSession() {
            return try next.respond(to: req)
        }
        let redirect = req.redirect(to: path)
        return req.eventLoop.newSucceededFuture(result: redirect)
    }
}
