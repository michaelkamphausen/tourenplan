import Vapor

class PromiseOperation<T>: AsynchronousOperation {
    typealias Block = (PromiseOperation) -> Future<T>
    
    let eventLoop: EventLoop
    let block: Block
    let promise: Promise<T>
    
    init(on eventLoop: EventLoop, block: @escaping Block) {
        self.eventLoop = eventLoop
        self.block = block
        self.promise = eventLoop.newPromise(of: T.self)
    }
    
    override func main() {
        guard !isCancelled && !isFinished else { return }
        
        block(self)
            .always({ self.finish() })
            .cascade(promise: promise)
    }
    
    override func cancel() {
        super.cancel()
        
        promise.fail(error: CustomError.cancelledRequest)
        finish()
    }
    
}

extension Future {
    
    func finishIfCancelled<U>(_ operation: PromiseOperation<U>) -> Future<T> {
        let promise = eventLoop.newPromise(T.self)
        
        self.do { expectation in
            if operation.isCancelled {
                operation.finish()
                promise.fail(error: CustomError.cancelledRequest)
            } else {
                promise.succeed(result: expectation)
            }
        }.catch { error in
            promise.fail(error: error)
        }
        
        return promise.futureResult
    }
    
}
