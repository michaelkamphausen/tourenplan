import Foundation

public class AsynchronousOperation: Operation {
    private enum State {
        case ready
        case executing
        case finished
        
        var key: String? {
            switch self {
            case .ready: return nil
            case .executing: return "isExecuting"
            case .finished: return "isFinished"
            }
        }
    }
    
    private var state = State.ready {
        willSet {
            let keys = [self.state, newValue].compactMap({ $0.key })
            keys.forEach({ self.willChangeValue(forKey: $0) })
        }
        didSet {
            let keys = [oldValue, self.state].compactMap({ $0.key })
            keys.forEach({ self.didChangeValue(forKey: $0) })
        }
    }
    
    override public var isAsynchronous: Bool {
        return true
    }
    override public var isExecuting: Bool {
        return state == .executing
    }
    override public var isFinished: Bool {
        return state == .finished
    }
    
    override public func start() {
        if isCancelled {
            state = .finished
        } else {
            state = .executing
            main()
        }
    }
    
    public func finish() {
        if !isFinished {
            state = .finished
        }
    }
}
