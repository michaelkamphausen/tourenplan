import Vapor
import Fuzi

class DMRZApiRequest {
    
    private static var operationQueueBySessionId = [String: SerialOperationQueueReference]()
    
    // MARK: - types
    
    enum Form: String {
        case medFrmLogin
        case medFrmUncleanLogoff
        case medFrmSuperPin
        case medFrmPrintNewTAN // /medFrmMain.vb?fmt=direct&action=printtanliste -> medFrmPrintNewTAN action:donePrintTAN <Data FormName="medFrmPrintNewTAN" LastAction="load" scw="1440" sch="394" ldtable="" valid="false"/>
        case medFrmPMOZahlungMethodeInfo
        case medFrmWIKIAssistent
        case medFrmTanInformation
        case medFrmTanEinrichtung
        case medFrmPAbwesenheit
        case medFrmPTourenplanung
    }
    
    // MARK: - constants
    
    static let baseUrl: URL = "https://svr01.dmrz.de/medFrmMain.vb"
    private static let userAgent = Environment.get("USER_AGENT") ?? ""
    private static let userAgentNotAccepted = "document.location=\"https://www.dmrz.de/wissen/ratgeber/browserumstellung\""
    private static let htmlHeaders: HTTPHeaders = ["User-Agent": userAgent]
    private static let xmlHeaders: HTTPHeaders = htmlHeaders.adding(name: "Content-Type", value: "application/xml")
    
    // MARK: - variables
    
    private let request: Request
    private let container: Container
    private let client: Client
    private(set) var sessionId: String
    private let operationQueue: OperationQueue
    private var currentForm = Form.medFrmPTourenplanung.rawValue
    private let initialAction: String = "goto" + Form.medFrmPTourenplanung.rawValue
    private var cookiesForRequest = ""
    
    // MARK: - object life cycle
    
    /// Important: this API is stateful per session, so a new method call will always cancel every ongoing method call for the same session with a CustomError.cancelledRequest
    init(request: Request, sessionId: String) throws {
        self.request = request
        self.container = request.subContainer(on: request)
        self.client = WrappedHTTPClient(on: self.container)
        self.sessionId = sessionId
        
        let queueReference = DMRZApiRequest.operationQueueBySessionId[sessionId] ?? SerialOperationQueueReference()
        queueReference.referenceCount += 1
        DMRZApiRequest.operationQueueBySessionId[sessionId] = queueReference
        self.operationQueue = queueReference.queue
    }
    
    deinit {
        if let queueReference = DMRZApiRequest.operationQueueBySessionId[sessionId] {
            queueReference.referenceCount -= 1

            if queueReference.referenceCount == 0 {
                DMRZApiRequest.operationQueueBySessionId.removeValue(forKey: sessionId)
            }
        }
    }
    
    // MARK: - public API methods
    
    func login(username: String, password: String) -> Future<((String, String), (Request.AuthenticationState, String?))> {
        return addOperation({ operation in
            let form = Form.medFrmLogin.rawValue
            let data = """
                <Data FormName="\(form)" login="\(username)" password="\(password)"/>
                """
            
            return self.post(action: "login", currentForm: form, data: data)
                .finishIfCancelled(operation)
                .map({ (response: Response) -> Response  in
                    self.cookiesForRequest = response.http.cookies.serializeForRequest()
                    return response
                })
                .body()
                .parse()
                .finishIfCancelled(operation)
                .flatMap(to: String.self, { document in
                    guard let sessionId = document.root?.attr("sid"),
                        let ngmseid = document.xpath("//Form").first?.attr("NGMSEID") else {
                        
                        if document.xpath("//script").first?.rawXML.contains(DMRZApiRequest.userAgentNotAccepted) == true {
                            return self.container.future(error: CustomError.userAgentNotAccepted)
                        } else {
                            return self.container.future(error: CustomError.missingData)
                        }
                    }
                    
                    self.sessionId = sessionId
                    
                    switch Form(rawValue: ngmseid) {
                    case .medFrmLogin?:
                        return self.container.future(error: CustomError.loginIncorrect)
                    case .medFrmUncleanLogoff?:
                        return self.post(action: "bestaetigen", currentForm: ngmseid)
                            .body()
                            .parse()
                            .flatMap({ document in
                                guard let ngmseid = document.xpath("//Form").first?.attr("NGMSEID") else {
                                    return self.container.future(error: CustomError.unexpectedResponse)
                                }
                                
                                return self.container.future(ngmseid)
                            })
                    default:
                        return self.container.future(ngmseid)
                    }
                    
                })
                .flatMap({ _ in
                    return self.post(action: "load", currentForm: Form.medFrmSuperPin.rawValue)
                })
                .flatMap({ _ in
                    let result = (self.sessionId, self.cookiesForRequest)
                    
                    // assuming that loading TAN labels is always required to authenticate , not just for form types medFrmSuperPin or medFrmWIKIAssistent
                    return self.container.future(result).and(self.loadTanLabel())
                })
        })
    }
    
    func login(tan: String, authState: Request.AuthenticationState) -> Future<(Request.AuthenticationState, String?)> {
        return addOperation({ operation in
            let tanType: String
            switch authState {
            case .requiresTan:
                tanType = "tan"
            case .requiresOTP:
                tanType = "tanverfahren"
            default:
                return self.container.future(error: CustomError.tanIncorrect)
            }
            
            let form = Form.medFrmSuperPin.rawValue
            let data = """
                <Data FormName="\(form)" \(tanType)="\(tan)"/>
                """
                
            return self.post(action: "dmrz\(tanType)", currentForm: form, data: data)
                .finishIfCancelled(operation)
                .validate()
                .parse()
                .finishIfCancelled(operation)
                .flatMap({ document in
                    return self.post(action: self.initialAction)
                })
                .finishIfCancelled(operation)
                .body()
                .parse()
                .finishIfCancelled(operation)
                .flatMap(self.handleUndesireableForms(self.initialAction, operation: operation))
                .flatMap({ _ in
                    return self.container.future((.authenticated, nil))
                })
                .catchFlatMap({ error in
                    guard (error as? CustomError) == .tanInputRequired else {
                        return self.container.future(error: error)
                    }
                    
                    return self.loadTanLabel()
                })
        })
    }
    
    func loginWithSessionId() -> Future<Void> {
        return addOperation({ operation in
            return self.post(action: self.initialAction)
                .finishIfCancelled(operation)
                .validate()
                .parse()
                .finishIfCancelled(operation)
                .flatMap(self.handleUndesireableForms(self.initialAction, operation: operation))
        })
    }
    
    func getRouteSchedule(_ date: Date) -> Future<RouteSchedule> {
        return addOperation({ operation in
            return self.goto(.medFrmPTourenplanung, operation: operation)
                .finishIfCancelled(operation)
                .flatMap(self.loadRouteSchedule(date, operation: operation))
                .flatMap({ tourGroups in
                    let future: Future<[Absence]>
                    
                    if tourGroups.isEmpty {
                        return self.container.future(error: CustomError.missingData)
                    }
                    
                    if let absences = self.request.absence {
                        future = self.container.future(absences)
                    } else {
                        future = self.gotoAbsenceAndBack(operation)
                    }
                    
                    return future.flatMap({ absences in
                        let absences = absences.filter({ date.isBetween($0.start, and: $0.end) })
                        let routeSchedule = RouteSchedule(tourGroups: tourGroups, absences: absences)

                        return self.container.future(routeSchedule)
                    })
                })
        })
    }
    
    func keepAlive() -> Future<Void> {
        return addOperation({ operation in
            return self.post(action: "showLD")
                .finishIfCancelled(operation)
                .validate()
                .void()
        })
    }
    
    // MARK: - private methods with all the details about loading and parsing data from DMRZ API
    
    private func handleUndesireableForms<T>(_ action: String, operation: PromiseOperation<T>) -> ((Fuzi.XMLDocument) -> Future<Void>) {
        return { document in
            guard let gmseid = document.xpath("//Form").first?.attr("GMSEID"),
                let ngmseid = document.xpath("//Form").first?.attr("NGMSEID") else {
                    return self.container.future(error: CustomError.unexpectedResponse)
            }
            
            guard gmseid == ngmseid else {
                if ngmseid == Form.medFrmPMOZahlungMethodeInfo.rawValue {
                    let data = """
                        <Data FormName="medFrmPMOZahlungMethodeInfo" LastAction="load" wikiaktion="WeiterleitungStartformZahlungMethode"/>
                        """
                    
                    return self.post(action: "wikiaktion", currentForm: ngmseid, data: data)
                        .finishIfCancelled(operation)
                        .body()
                        .parse(regex: "<Form.*?\\/>")
                        .finishIfCancelled(operation)
                        .flatMap({ document in
                            guard let ngmseid = document.xpath("//Form").first?.attr("NGMSEID") else {
                                return self.container.future(error: CustomError.unexpectedResponse)
                            }
                            
                            return self.post(action: action, currentForm: ngmseid).void()
                        })
                } else if ngmseid == Form.medFrmTanInformation.rawValue {
                    let data = """
                        <Data FormName="medFrmTanInformation" LastAction="load" wikiaktion="AbbrechenTanInfo"/>
                        """
                    
                    return self.post(action: "wikiaktion", currentForm: ngmseid, data: data)
                        .finishIfCancelled(operation)
                        .body()
                        .parse()
                        .finishIfCancelled(operation)
                        .flatMap(self.handleUndesireableForms(self.initialAction, operation: operation))
                } else {
                    return self.post(action: action, currentForm: ngmseid).void()
                }
            }
            
            return self.container.future()
        }
    }
    
    private func loadTanLabel() -> Future<(Request.AuthenticationState, String?)> {
        return self.get()
            .body()
            .parse()
            .flatMap({ document in
                if let element = document.xpath("//*[@id='fstanverfahren2' or @id='fsTANLogin']").first,
                    !element.attr("style").valueOrEmpty().contains("display:none") {
                        return self.container.future((.requiresOTP, "Geben Sie die in Ihrem TAN-Generator angezeigte TAN ein:"))
                } else if let element = document.xpath("//*[@id='fstan2']").first,
                    !element.attr("style").valueOrEmpty().contains("display:none"),
                    let tanLabel = element.xpath("*[@id='tan2']").first?.stringValue,
                    !tanLabel.isEmpty {
                        return self.container.future((.requiresTan, tanLabel))
                } else if let element = document.xpath("//*[@id='divForm']").first,
                    !element.attr("style").valueOrEmpty().contains("display:none") {
                        return self.container.future(error: CustomError.newPasswordRequired)
                } else {
                    return self.container.future(error: CustomError.missingData)
                }
            })
    }
    
    @discardableResult
    private func goto<T>(_ form: Form, operation: PromiseOperation<T>) -> Future<Void> {
        let action = "goto" + form.rawValue.lowercased()
        
        return post(action: action)
            .finishIfCancelled(operation)
            .validate()
            .parse()
            .finishIfCancelled(operation)
            .flatMap(to: Fuzi.XMLDocument.self, { document in
                guard let gmseid = document.xpath("//Form").first?.attr("GMSEID"),
                    let ngmseid = document.xpath("//Form").first?.attr("NGMSEID") else {
                        return self.container.future(error: CustomError.unexpectedResponse)
                }
                
                guard gmseid == ngmseid else {
                    return self.post(action: action, currentForm: ngmseid)
                        .finishIfCancelled(operation)
                        .body()
                        .parse()
                }
                
                return self.container.future(document)
            })
            .finishIfCancelled(operation)
            .flatMap(to: Void.self, { document in
                guard form.rawValue == self.currentForm,
                    document.xpath("//Data").first != nil,
                    document.xpath("//DataNode/ldtable").first != nil else {
                        self.currentForm = form.rawValue
                        return self.get().void()
                }
                
                return self.container.future()
            })
    }
    
    @discardableResult
    private func gotoAbsenceAndBack<T>(_ operation: PromiseOperation<T>) -> Future<[Absence]> {
        return self.goto(.medFrmPAbwesenheit, operation: operation)
            .finishIfCancelled(operation)
            .flatMap(self.loadAbsence(operation))
            .finishIfCancelled(operation)
            .map(to: [Absence].self, { results in
                self.request.absence = results
                self.goto(.medFrmPTourenplanung, operation: operation)
                
                return results
            })
    }
    
    private func loadAbsence<T>(_ operation: PromiseOperation<T>) -> ((Any) -> Future<[Absence]>) {
        return { _ in
            self.post(action: "load")
                .finishIfCancelled(operation)
                .validate()
                .parse()
                .finishIfCancelled(operation)
                .map(to: [Absence].self, { document in
                    let rows = document.xpath("//DataNode/ldtable//tr[@id]")
                    document.dateFormatter.dateFormat = "dd.MM.yyyy"
                    
                    return rows.map({ element in
                        // TODO: load all pages: is it relevant?
                        // TODO: evaluate time: is it relevant?
                        let start = element.firstChild(xpath: "td[@xcn='datumvon']")?.dateValue ?? Date.distantPast
                        let end = element.firstChild(xpath: "td[@xcn='datumbis']")?.dateValue ?? Date.distantFuture
                        
                        let name = [element.firstChild(xpath: "td[@xcn='v_vvorname']")?.stringValue.abbr, element.firstChild(xpath: "td[@xcn='v_vnachname']")?.stringValue].compactMap({ $0}).joined(separator: " ")
                        
                        let reason: Absence.Reason
                        switch element.firstChild(xpath: "td[@xcn='v_pabwesenheittypbezeichnung']")?.stringValue {
                        case "Kurzzeitpflege":
                            reason = .shortTermCare
                        case "Krankenhaus":
                            reason = .hospital
                        default:
                            reason = .other
                        }
                        
                        return Absence(name: name, reason: reason, start: start, end: end)
                    })
                })
        }
    }
    
    private func loadRouteScheduleIds<T>(_ date: Date, operation: PromiseOperation<T>) -> Future<[Date: [String]]> {
        let cachedRouteScheduleIds = self.request.dmrzRouteScheduleIds
        
        if let cachedRouteScheduleIds = cachedRouteScheduleIds,
            cachedRouteScheduleIds[date] != nil {
                return self.container.future(cachedRouteScheduleIds)
        } else {
            let dateComponents = [Calendar.Component.year, .month, .day]
                .map({ String(Calendar.current.component($0, from: date)) })
            let formattedDate = dateComponents.joined(separator: "/") + "/0/0/0"
            let data = "<Data FormName=\"\(Form.medFrmPTourenplanung.rawValue)\" caldatselect=\"\(formattedDate)\" d=\"\(formattedDate)\"/>"
            
            return self.post(action: "kalmovetotargetdate", data: data)
                .finishIfCancelled(operation)
                .flatMap({ _ in self.post(action: "showLD") })
                .finishIfCancelled(operation)
                .validate()
                .flatMap(to: (Response, String).self, { arguments in
                    let (response, body) = arguments
                    
                    guard !body.contains("NGMSEID=\"\(Form.medFrmPrintNewTAN)\"") else {
                        return response.future(error: CustomError.tanListDownloadRequired)
                    }
                    guard !body.contains("NGMSEID=\"\(Form.medFrmTanEinrichtung)\"") else {
                        return response.future(error: CustomError.tanSetupRequired)
                    }
                    
                    return response.future(arguments)
                })
                .parse()
                .finishIfCancelled(operation)
                .flatMap({ document in
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "'tx_'yyyy'.'M'.'d"
                    let entries: [(Date, [String])] = document.xpath("//div[starts-with(@id,'tx_')]")
                        .map({ element in
                            let id: String = element.attr("id").valueOrEmpty()
                            let date: Date = dateFormatter.date(from: id).value(or: Date.distantPast)

                            let rowIds: [String] = element.xpath("div[@onclick]").compactMap({ element in
                                if let value = element.attr("onclick")?.split(separator: "'")[safe: 1] {
                                    return String(value)
                                } else {
                                    return nil
                                }
                            })
                            return (date, rowIds)
                        })
                    let routeScheduleIds = entries.reduce(into: [Date: [String]](), { $0[$1.0] = $1.1 })
                    
                    if let cachedRouteScheduleIds = cachedRouteScheduleIds {
                        self.request.dmrzRouteScheduleIds = cachedRouteScheduleIds.merging(routeScheduleIds) { (_, new) in new }
                    } else {
                        self.request.dmrzRouteScheduleIds = routeScheduleIds
                    }
                    
                    return self.container.future(routeScheduleIds)
                })
        }
    }
    
    private func loadRouteScheduleData<T>(_ id: String, weekday: String, includeActions: Bool, includeDurations: Bool, operation: PromiseOperation<T>) -> Future<[Tour]> {
        var tourAttributes = [String: String]()
        
        return self.post(action: "edittp", formAttributes: "RowID=\"\(id)\"")
            .finishIfCancelled(operation)
            .body()
            .parse()
            .finishIfCancelled(operation)
            .flatMap(to: ([(TourStop, String)], NodeSet).self, { document in
                guard let patientSection = document.xpath("//table[@Name='dmrzpteinsatz']").first,
                    let tourSection = document.xpath("//table[@Name='dmrzptour']").first,
                    case let patientAttributes = patientSection.xpath("tableinfo/cols/col")
                        .map({ ($0.attr("Caption").valueOrEmpty(), "c" + $0.attr("i").valueOrEmpty()) })
                        .reduce(into: [:], { $0[$1.0.uppercased()] = $1.1 }),
                    !patientAttributes.isEmpty,
                    case let tourCols = tourSection.xpath("tableinfo/cols/col")
                        .map({ ($0.attr("Caption").valueOrEmpty(), "c" + $0.attr("i").valueOrEmpty()) })
                        .reduce(into: [:], { $0[$1.0.uppercased()] = $1.1 }),
                    !tourCols.isEmpty,
                    case let patientRows = patientSection.xpath("rows/row"),
                    !patientRows.isEmpty,
                    case let tourRows = tourSection.xpath("rows/row"),
                    !tourRows.isEmpty else {
                        return self.container.future(([], document.xpath("NEVEREVER")))
                }
                
                tourAttributes = tourCols
                let modusAttribute = patientAttributes["MODUS"].valueOrEmpty()
                let dmrzTourIdAttribute = patientAttributes["DMRZPTOUR_ID"].valueOrEmpty()
                let dmrzEinsatzIdAttribute = patientAttributes["DMRZPTEINSATZ_ID"].valueOrEmpty()
                
                let futurePatientsWithTourId = patientRows.compactMap({ row -> Future<(TourStop, String)>? in
                    guard let patientOrCommitment = row.attr(modusAttribute),
                        let tourId = row.attr(dmrzTourIdAttribute),
                        let commitmentId = row.attr(dmrzEinsatzIdAttribute) else {
                            return nil
                    }
                    
                    let remarks = row.attr(patientAttributes["BEZEICHNUNG"].valueOrEmpty())
                    
                    // patient
                    if patientOrCommitment == "0" {
                        guard let firstName = row.attr(patientAttributes["V_VERSICHERTENVORNAME"].valueOrEmpty()),
                            let lastName = row.attr(patientAttributes["V_VERSICHERTENNACHNAME"].valueOrEmpty()) else {
                                return nil
                        }
                        
                        var actions = [TourStop.Action]()
                        let name = [firstName.abbr, lastName].compactMap({ $0 }).joined(separator: " ")
                        
                        guard !name.isEmpty else { return nil }
                        
                        
                        let duration: Int?
                        if (includeDurations) {
                            duration = Int(row.attr(patientAttributes["DAUER"].valueOrEmpty()).valueOrEmpty())
                        } else {
                            duration = nil
                        }
                        
                        if (includeActions) {
                            if remarks?.lowercased().contains("duschen") == true {
                                actions.append(.shower)
                            }
                            if remarks?.lowercased().contains("baden") == true {
                                actions.append(.bath)
                            }
                            
                            return self.client.post(DMRZApiRequest.baseUrl, headers: DMRZApiRequest.xmlHeaders, beforeSend: { request in
                                    self.setDmrzCookies(request)
                                    request.http.body = """
                                        <gmse sid="\(self.sessionId)"><Form autoly="0" action="getleistungen" SaveState="true" GMSEID="\(Form.medFrmPTourenplanung.rawValue)" accept="xml" fmt="xml" einsatzid=\"\(commitmentId)\"/></gmse>
                                        """.convertToHTTPBody()
                                })
                                .flatMap(to: (TourStop, String).self, { response in
                                    return DMRZApiRequest.body(response)
                                        .parse(regex: "<table Name=\"leistungen\".*?<\\/table>")
                                        .map({ document in
                                            actions += document.xpath("//row")
                                                .compactMap({ TourStop.Action(description: $0.attr("beschreibung").valueOrEmpty()) })
                                            
                                            return (TourStop(kind: .patient, name: name, actions: Array(Set(actions)), address: nil, remarks: remarks, durationMinutes: duration), tourId)
                                        })
                                        .catchMap({ error in
                                            actions.append(TourStop.Action.warning)
                                            return (TourStop(kind: .patient, name: name, actions: Array(Set(actions)), address: nil, remarks: remarks, durationMinutes: duration), tourId)
                                        })
                                })
                        } else {
                            return self.container.future((TourStop(kind: .patient, name: name, actions: [], address: nil, remarks: remarks, durationMinutes: duration), tourId))
                        }
                    // other commitment
                    } else if patientOrCommitment == "1" {
                        guard let kindNumberAsString = row.attr(patientAttributes["SUBMODUS"].valueOrEmpty()),
                            let kindNumber = Int(kindNumberAsString),
                            let kind = TourStop.Kind(rawValue: kindNumber),
                            let remarks = remarks,
                            let hasAddressAsString = row.attr(patientAttributes["HATAPADRESSE"].valueOrEmpty()) else {
                                return nil
                        }
                        
                        let hasAddress = hasAddressAsString != "0" // "-1" if it has an address 🤷‍♂️
                        let address: String?
                        
                        if (hasAddress) {
                            let name1 = row.attr(patientAttributes["V_ABWRGANNAME"].valueOrEmpty())
                            let name2 = row.attr(patientAttributes["V_ABWRGANNAME2"].valueOrEmpty())
                            let street = row.attr(patientAttributes["V_ABWRGANSTRASSE"].valueOrEmpty()).valueOrEmpty()
                            let zipCode = row.attr(patientAttributes["V_ABWRGANPLZ"].valueOrEmpty())
                            let city = row.attr(patientAttributes["V_ABWRGANORT"].valueOrEmpty())
                            let name = [name1, name2].compactMap({ $0 }).joined(separator: " ")
                            let zipCodeCity = [zipCode, city].compactMap({ $0 }).joined(separator: " ")
                            address = [name, street, zipCodeCity].filter({ !$0.isEmpty }).joined(separator: ", ")
                        } else {
                            address = nil
                        }
                        
                        return self.container.future((TourStop(kind: kind, name: remarks, actions: [], address: address, remarks: nil, durationMinutes: nil), tourId))
                        
                    // neither patient nor commitment
                    } else {
                        return nil
                    }
                })
                
                return Future.whenAll(futurePatientsWithTourId, eventLoop: self.container.eventLoop).and(result: tourRows)
            })
            .flatMap(to: [Tour].self, { arguments in
                let (patientsWithTourId, tourRows) = arguments
                let patientsById = Dictionary(grouping: patientsWithTourId, by: { $0.1 })
                
                let tours = tourRows.compactMap({ row -> Tour? in
                    guard let start = row.attr(tourAttributes["STARTZEIT"].valueOrEmpty()),
                        let firstName = row.attr(tourAttributes["V_PUSERVORNAME"].valueOrEmpty()),
                        let lastName = row.attr(tourAttributes["V_PUSERNAME"].valueOrEmpty()),
                        let tourId = row.attr(tourAttributes["DMRZPTOUR_ID"].valueOrEmpty()),
                        let patients = patientsById[tourId]?.map({ $0.0 }) else {
                            return nil
                    }
                    
                    let nurseName = [firstName, lastName.abbr].compactMap({ $0 }).joined(separator: " ")
                    let vehicle = row.attr(tourAttributes["V_FAHRZEUGBEZEICHNUNG"].valueOrEmpty()).valueOrEmpty()
                    
                    return Tour(start: start, nurseName: nurseName, vehicle: vehicle, isLead: false, stops: patients, weekday: weekday)
                })
                
                return self.container.future(tours)
            })
    }
    
    // This method interprets loaded route schedule data in a very specific way and should be customizable for different organisations
    private func loadRouteSchedule<T>(_ date: Date, operation: PromiseOperation<T>) -> ((Any) -> Future<[TourGroup]>) {
        return { _ in
            let localizedWeekdays = [1: "Sonntag", 2: "Montag", 3: "Dienstag", 4: "Mittwoch", 5: "Donnerstag", 6: "Freitag", 7: "Samstag"]
            let weekdayNumber = Calendar.current.dateComponents([.weekday], from: date).weekday.value(or: 2)
            let weekday = localizedWeekdays[weekdayNumber].valueOrEmpty()
            
            return self.loadRouteScheduleIds(date, operation: operation)
                .flatMap({ routeScheduleIds in
                    guard let rowIds = routeScheduleIds[date],
                        !rowIds.isEmpty else {
                            return self.container.future(error: CustomError.unexpectedResponse)
                    }
                    
                    let future: Future<[TourGroup]> = self.loadRouteScheduleData(rowIds[0], weekday: weekday, includeActions: true, includeDurations: false, operation: operation)
                        .flatMap({ tours in
                            var titles = ["Frühdienst", "Spätdienst"]
                            let toursByTitle = Dictionary<String, [Tour]>(grouping: tours, by: { tour in
                                let hour = Int(String(tour.start.split(separator: "/")[safe: 3] ?? "")).value(or: 0)
                                
                                if hour < 12 {
                                    return titles[0]
                                } else {
                                    return titles[1]
                                }
                            })
                            
                            
                            let tourGroups: [TourGroup] = titles.compactMap({ title in
                                if let tours = toursByTitle[title] {
                                    let tourLists = tours.map({ TourList(title: nil, tours: [$0]) })
                                    return TourGroup(title: title, tourLists: tourLists)
                                } else {
                                    return nil
                                }
                            })
                            titles = titles.filter({ toursByTitle[$0] != nil })
                            return self.container.future(tourGroups)
                        })
                    
                    if rowIds.count > 1 {
                        let calendar = Calendar.current
                        var weekdayOfToday = calendar.dateComponents([.weekday], from: date).weekday.value(or: 2)
                        if weekdayOfToday == 1 {
                            weekdayOfToday += 7 // week shall start on Monday
                        }
                        let futureToursForWeek: [Future<[Tour]>] = Array(2...8).compactMap({ weekday in
                            guard let date = calendar.date(byAdding: .day, value: weekday - weekdayOfToday, to: date),
                                let rowId = routeScheduleIds[date]?[safe: 1] else {
                                    return nil
                            }
                            
                            let weekday = localizedWeekdays[((weekday - 1) % 7) + 1].valueOrEmpty()
                            return self.loadRouteScheduleData(rowId, weekday: weekday, includeActions: false, includeDurations: false, operation: operation)
                        })
                        return future
                            .and(Future.whenAll(futureToursForWeek, eventLoop: self.container.eventLoop))
                            .flatMap({ arguments in
                                let (tourGroups, tours) = arguments
                                
                                if !tours.isEmpty {
                                    let tourLists: [TourList] = tours.compactMap({ tours in
                                        if !tours.isEmpty {
                                            let condensedTours: [Tour] = tours.map({ tour in
                                                let stops: [TourStop] = tour.stops.reduce([], { stops, stop in
                                                    var stops = stops

                                                    if stops.last?.kind == .patient, stop.kind == .patient, stops.last?.name == stop.name {
                                                        let last = stops.removeLast()
                                                        stops.append(TourStop(kind: last.kind, name: last.name, actions: last.actions, address: last.address, remarks: last.remarks, durationMinutes: last.durationMinutes.value(or: 0) + stop.durationMinutes.value(or: 0)))
                                                    } else {
                                                        stops.append(stop)
                                                    }
                                                    return stops
                                                })
                                                return Tour(start: tour.start, nurseName: tour.nurseName, vehicle: tour.vehicle, isLead: tour.isLead, stops: stops, weekday: tour.weekday)
                                            })
                                            return TourList(title: tours.first?.weekday, tours: condensedTours)
                                        } else {
                                            return nil
                                        }
                                    })
                                    let tourGroup = TourGroup(title: "HW + Betr.", tourLists: tourLists)
                                    return self.container.future(tourGroups + [tourGroup])
                                } else {
                                    return self.container.future(tourGroups)
                                }
                            })
                    } else {
                        return future
                    }
                })
        }
    }
    
    // MARK: - private operation queue based methods
    
    private func addOperation<T>(_ block: @escaping PromiseOperation<T>.Block) -> Future<T> {
        operationQueue.cancelAllOperations()
        let operation = PromiseOperation<T>(on: container.eventLoop, block: block)
        operationQueue.addOperation(operation)
        
        return operation.promise.futureResult
    }
    
    // MARK: - private request maker methods
    
    @discardableResult
    private func get() -> Future<Response> {
        let baseUrl = DMRZApiRequest.baseUrl.addQueryItems([URLQueryItem(name: "sid", value: sessionId)])
        
        return self.client.get(baseUrl, headers: DMRZApiRequest.htmlHeaders) { request in
            self.setDmrzCookies(request)
        }
    }
    
    private func post(action: String, currentForm: String? = nil, formAttributes: String = "", data: String = "") -> Future<Response> {
        let currentForm = currentForm.value(or: self.currentForm)
        
        return self.client.post(DMRZApiRequest.baseUrl, headers: DMRZApiRequest.xmlHeaders) { request in
            self.setDmrzCookies(request)
            request.http.body = """
                <gmse sid="\(self.sessionId)"><Form autoly="0" action="\(action)" SaveState="true" GMSEID="\(currentForm)" accept="xml" fmt="xml" \(formAttributes)/>\(data)</gmse>
                """.convertToHTTPBody()
        }
    }
    
    // MARK: - private request helper methods
    
    private func setDmrzCookies(_ request: Request) {
        let cookies = self.request.dmrzCookies ?? self.cookiesForRequest
        if !cookies.isEmpty {
            request.http.headers.replaceOrAdd(name: .cookie, value: cookies)
        }
    }
    
}

// MARK: - private static response handler methods

extension DMRZApiRequest {
    
    fileprivate static func body(_ response: Response) -> Future<(Response, String)> {
        guard response.http.status != .serviceUnavailable else {
            return response.future(error: CustomError.unavailable)
        }
        
        guard response.http.status == .ok,
            let data = response.http.body.data,
            let body = String(data: data, encoding: .utf8) else {
                return response.future(error: CustomError.unexpectedResponse)
        }
        
        return response.future((response, body))
    }
    
    fileprivate static func validate(_ response: Response, body: String) -> Future<(Response, String)> {
        guard let contentType = response.http.contentType?.subType else {
            return response.future(error: CustomError.unexpectedResponse)
        }
        
        if contentType == "xml" {
            guard !body.contains("sidstate=\"terminated\"") &&
                !body.contains("NGMSEID=\"medFrmLogin\"") else {
                    return response.future(error: CustomError.loginRequired)
            }
            guard !body.contains("NGMSEID=\"medFrmSuperPin\"") else {
                return response.future(error: CustomError.tanInputRequired)
            }
        } else {
            guard !body.contains("id=\"tbLogin\"") else {
                return response.future(error: CustomError.loginRequired)
            }
            
            guard !body.contains("id=\"inputtan\"") else {
                return response.future(error: CustomError.tanInputRequired)
            }
        }
        
        return response.future((response, body))
    }
    
    fileprivate static func parse(_ regex: String? = nil) -> ((Response, String) -> Future<Fuzi.XMLDocument>) {
        return { response, body in
            guard let document = try? XMLDocument(string: body.substring(regex: regex)) else {
                return response.future(error: CustomError.unexpectedResponse)
            }
            
            return response.future(document)
        }
    }
    
}

// MARK: - syntactic sugar

extension Future {
    
    func void() -> Future<Void> {
        return map({ _ in () })
    }
    
    func body() -> Future<(Response, String)> {
        return mapResponse(DMRZApiRequest.body)
    }
    
    func validate() -> Future<(Response, String)> {
        return mapResponse({ response in
            return DMRZApiRequest.body(response).flatMap(DMRZApiRequest.validate)
        })
    }
    
    func parse(regex: String? = nil) -> Future<Fuzi.XMLDocument> {
        return mapResponse(DMRZApiRequest.parse(regex))
    }
    
    private func mapResponse<T, U>(_ callback: @escaping (T) -> (Future<U>)) -> Future<U> {
        let promise = eventLoop.newPromise(U.self)
        
        self.do({ expectation in
            if let expectation = expectation as? T {
                return callback(expectation).cascade(promise: promise)
            } else {
                fatalError("expected Response type but found \(expectation.self)")
            }
        }).catch({ error in
            promise.fail(error: error)
        })
        
        return promise.futureResult
    }
    
}

extension HTTPHeaders {
    
    func adding(name: String, value: String) -> HTTPHeaders {
        var copy = self
        copy.add(name: name, value: value)
        return copy
    }
    
}
