import Vapor

class WrappedHTTPClient: Client {
    
    /// See `Client`.
    public let container: Container
    
    /// Creates a new `BasicHTTPClient`.
    public init(on container: Container) {
        self.container = container
    }
    
    /// See `Client`.
    public func send(_ req: Request) -> EventLoopFuture<Response> {
        guard let scheme = req.http.url.scheme,
            let hostname = req.http.url.host else {
                return container.future(error: CustomError.invalidUrl)
        }
        
        let httpScheme: HTTPScheme
        switch scheme {
        case "https":
            httpScheme = .https
        case "http":
            httpScheme = .http
        case "wss":
            httpScheme = .wss
        case "ws":
            httpScheme = .ws
        default:
            return container.future(error: CustomError.invalidUrl)
        }
        
        return HTTPClient.connect(scheme: httpScheme, hostname: hostname, on: container, onError: { error in
            print("HTTPClient Connection Error:", error)
        }).flatMap(to: (HTTPResponse, HTTPClient).self, { client in
            return client.send(req.http).and(result: client)
        }).map(to: Response.self, { arguments in
            let (httpResponse, client) = arguments
            _ = client.close()
            return Response(http: httpResponse, using: self.container)
        })
    }
    
}
