import Foundation

class SerialOperationQueueReference: CustomDebugStringConvertible {
    let queue: OperationQueue
    var referenceCount: UInt = 0
    var debugDescription: String {
        return "(references: \(referenceCount), operations: \(queue.operationCount))"
    }
    
    init() {
        queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
    }
}
