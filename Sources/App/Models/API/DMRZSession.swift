import Vapor

extension Request {
    
    enum AuthenticationState: String {
        case unauthenticated
        case requiresTan
        case requiresOTP
        case authenticated
    }
    
    var dmrzAuthenticationState: AuthenticationState {
        let rawValue = (session?["dmrzAuthenticationState"]).valueOrEmpty()
        return AuthenticationState(rawValue: rawValue).value(or: .unauthenticated)
    }
    var dmrzSessionId: String? {
        return session?["dmrzSessionId"]
    }
    var dmrzUrl: String? {
        return session?["dmrzUrl"]
    }
    var dmrzTanLabel: String? {
        return session?["dmrzTanLabel"]
    }
    var dmrzCookies: String? {
        return session?["dmrzCookies"]
    }
    var dmrzRouteScheduleIds: [Date: [String]]? {
        get {
            return decode("dmrzRouteScheduleIds")
        }
        set {
            encode("dmrzRouteScheduleIds", value: newValue)
        }
    }
    var absence: [Absence]? {
        get {
            return decode("absence")
        }
        set {
            encode("absence", value: newValue)
        }
    }
    private var session: Session? {
        if (try? hasSession()).value(or: false) {
            return try? session()
        } else {
            return nil
        }
    }
    
    func hasAuthenticatedDmrzSession() throws -> Bool {
        return try hasSession() && (dmrzAuthenticationState == .authenticated)
    }
    
    func authenticateDmrzSession(sessionId: String, url: String, authState: AuthenticationState, tanLabel: String? = nil, cookies: String? = nil) throws {
        let session = try self.session()
        session["dmrzAuthenticationState"] = authState.rawValue
        session["dmrzSessionId"] = sessionId
        session["dmrzUrl"] = url
        session["dmrzTanLabel"] = tanLabel
        session["dmrzCookies"] = cookies
    }
    
    func authenticateDmrzSession(authState: AuthenticationState, tanLabel: String?) throws {
        guard dmrzAuthenticationState != .unauthenticated else { return }
        
        let session = try self.session()
        
        session["dmrzAuthenticationState"] = authState.rawValue
        session["dmrzTanLabel"] = tanLabel
    }
    
    func unauthenticateDmrzSession() throws {
        let session = try self.session()
        session.data = SessionData()
        try destroySession()
    }
    
    private func decode<T>(_ key: String) -> T? where T: Decodable {
        guard let session = try? self.session(),
            let json = session[key],
            let data = json.data(using: .utf8) else { return nil }
        
        do {
            let value = try JSONDecoder().decode(T.self, from: data)
            return value
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
    
    private func encode<T>(_ key: String, value: T?) where T: Encodable {
        do {
            let session = try self.session()
            
            guard let value = value else {
                session[key] = nil
                return
            }
            
            let data = try JSONEncoder().encode(value)
            
            if let json = String(data: data, encoding: .utf8) {
                session[key] = json
            }
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
    
}

extension HTTPCookies {
    
    public func serializeForRequest() -> String {
        return all.map { (name, value) in
            return "\(name)=\(value.string)"
        }.joined(separator: "; ")
    }
    
}
