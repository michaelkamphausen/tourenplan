import Vapor

enum Route: String {
    case start
    
    case login
    case loginWithDMRZUsername
    case loginWithDMRZTan
    case loginWithDMRZSession
    case logout
    case keepAlive
    
    case routeSchedule
    
    case aboutUs
    case privacyPolicy
    case openSource
    
    var path: String {
        switch self {
        case .start: return "/"
            
        case .login: return "/auth"
        case .loginWithDMRZUsername: return "/auth/dmrz/login"
        case .loginWithDMRZTan: return "/auth/dmrz/tan"
        case .loginWithDMRZSession: return "/auth/dmrz/session"
        case .logout: return "/auth/logout"
        case .keepAlive: return "/auth/keepalive"
            
        case .routeSchedule: return "/tourenplanung"
            
        case .aboutUs: return "/impressum"
        case .privacyPolicy: return "/datenschutz"
        case .openSource: return "/open-source"
        }
    }
}

extension Route: PathComponentsRepresentable {
    public func convertToPathComponents() -> [PathComponent] {
        return path.split(separator: "/").map { .constant(.init($0)) }
    }
}

extension Request {
    func redirect(to location: Route, type: RedirectType = .normal) -> Response {
        return redirect(to: location.path, type: type)
    }
}
