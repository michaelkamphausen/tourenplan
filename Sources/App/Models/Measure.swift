import Vapor

#if DEBUG

class Measure: CustomDebugStringConvertible {
    
    struct Record: CustomDebugStringConvertible {
        let type: String
        let label: String?
        let start: DispatchTime
        let end: DispatchTime
        var duration: TimeInterval {
            return Measure.duration(start, to: end)
        }
        var debugDescription: String {
            return "\([type, label].compactMap({ $0 }).joined(separator: " ")): \(duration.ms)"
        }
    }
    
    static func duration(_ start: DispatchTime, to end: DispatchTime = DispatchTime.now()) -> TimeInterval {
        if start <= end {
            let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
            return TimeInterval(nanoTime) / 1_000_000_000
        } else {
            let nanoTime = start.uptimeNanoseconds - end.uptimeNanoseconds
            return -TimeInterval(nanoTime) / 1_000_000_000
        }
    }
    
    var records = [Record]()
    var debugDescription: String {
        guard let first = records.first,
            let last = records.last else { return "Measure: none" }
        
        let total = Measure.duration(first.start, to: last.end)
        let differences = zip(records, records.dropFirst()).map({ Measure.duration($0.end, to: $1.start) }) + [0]
        let list = zip(records, differences).map({ "  \($0.debugDescription), until next: \($1.ms)" })
        let groups = Dictionary.init(grouping: records, by: { $0.type })
            .map({ "  \($0): \($1.reduce(0, { $0 + $1.duration }).ms)" }) +
            ["  until next: \(differences.reduce(0, +).ms)"]
        
        return "Measure (total: \(total.ms)):\n" +
            list.joined(separator: "\n") + "\n" +
            "by types:\n" + groups.joined(separator: "\n")
    }
    
    @discardableResult
    func sync<T>(type: String = "", label: String? = nil, block: @autoclosure () -> (T)) -> T {
        let start = DispatchTime.now()
        let result = block()
        let end = DispatchTime.now()
        records.append(Record(type: type, label: label, start: start, end: end))
        return result
    }
    
}

extension Future {
    
    func measure<T, U>(measure: Measure, type: String = "", label: String? = nil, block: @escaping (T) -> (Future<U>)) -> Future<U> {
        let promise = eventLoop.newPromise(U.self)
        
        self.do { expectation in
            if let expectation = expectation as? T {
                let start = DispatchTime.now()
                block(expectation)
                    .always({
                        let end = DispatchTime.now()
                        measure.records.append(Measure.Record(type: type, label: label, start: start, end: end))
                    })
                    .cascade(promise: promise)
            } else {
                fatalError("expected Response type but found \(expectation.self)")
            }
        }.catch { error in
            promise.fail(error: error)
        }
        
        return promise.futureResult
    }
    
}

extension TimeInterval {
    
    var ms: String {
        return "\(Int(self * 1000)) ms"
    }
    
}

#endif
