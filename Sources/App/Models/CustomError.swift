import Vapor

enum CustomError: String, Debuggable {
    case invalidUrl
    case missingSessionId
    case unexpectedResponse
    case loginRequired
    case tanInputRequired
    case loginIncorrect
    case tanIncorrect
    case tanListDownloadRequired
    case newPasswordRequired
    case userAgentNotAccepted
    case missingData
    case unavailable
    case cancelledRequest
    case tanSetupRequired
    
    var identifier: String {
        return rawValue
    }
    
    var reason: String {
        switch self {
        default:
            return "unknown reason"
        }
    }

}
