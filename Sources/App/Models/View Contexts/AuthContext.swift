import Vapor

struct AuthContext {
    enum AuthType: String, Content {
        case login
        case tan
        case session
    }
    
    let type: AuthType
    let authType: String
    let dmrzUrl: String
    let error: String?
    let tanLabel: String?
    let login: DMRZLoginRequest?
    
    init(authType: AuthType = .login, dmrzUrl: URL = DMRZApiRequest.baseUrl, error: Error? = nil, tanLabel: String? = nil, login: DMRZLoginRequest? = nil) {
        self.type = authType
        self.authType = "auth" + authType.rawValue.capitalized
        self.dmrzUrl = dmrzUrl.absoluteString
        self.error = AuthContext.localizedDescription(error: error, type: authType, url: dmrzUrl)
        self.tanLabel = tanLabel
        self.login = login
    }
    
}

extension AuthContext: Content {}

extension AuthContext {
    
    private static func localizedDescription(error: Error?, type: AuthType, url: URL) -> String? {
        if let error = error as? CustomError {
            let externalLinkAttributes = "href=\"\(url)\" target=\"_blank\" rel=\"noopener noreferrer nofollow\""
            let externalLinkIcon = "<i class=\"fas fa-external-link-alt\"></i>"
            switch error {
            case .invalidUrl, .missingSessionId:
                return "Das ist leider keine gültige DMRZ-Web-Adresse. <a \(externalLinkAttributes)>Bitte loggen Sie sich zuerst bei DRMZ \(externalLinkIcon) ein</a> und kopieren Sie dort anschließend den vollständigen Inhalt der Adresszeile Ihres Browsers und fügen Sie diesen hier ein."
            case .tanInputRequired:
                return "Diese Anmeldung bei DRMZ ist noch nicht komplett. <a \(externalLinkAttributes)>Bitte geben Sie die gefragte TAN in DRMZ \(externalLinkIcon) ein</a>."
            case .loginRequired:
                switch type {
                case .session:
                    return "Diese DRMZ-Sitzung ist abgelaufen. <a \(externalLinkAttributes)>Bitte melden Sie sich erneut bei DRMZ \(externalLinkIcon) an</a>."
                case .login:
                    return "Die DRMZ-Sitzung ist abgelaufen. Bitte melden Sie sich erneut an."
                case .tan:
                    return "Die Zeit für die TAN-Eingabe ist leider abgelaufen, wir müssen noch einmal <a href=\"\(Route.logout)\">von vorne beginnen</a>."
                }
            case .loginIncorrect:
                return "Die DRMZ-Zugangsdaten stimmen nicht. Bitte prüfen Sie die Angaben und versuchen Sie es erneut."
            case .tanIncorrect:
                return "<p>Die DRMZ-TAN ist nicht korrekt. Bitte prüfen Sie die TAN und versuchen Sie es erneut.</p><p class=\"small mb-0\">Falls sich der Fehler wiederholt, obwohl die TAN korrekt war, <a \(externalLinkAttributes)>loggen Sie sich bitte bei DMRZ \(externalLinkIcon) ein</a>, denn in dem Fall könnte das Passwort abgelaufen sein.</p>"
            case .unavailable:
                return "DMRZ ist momentan nicht erreichbar."
            case .cancelledRequest:
                return "Der Anmeldeversuch wurde abgebrochen. Bitte erneut versuchen."
            case .userAgentNotAccepted:
                return "DMRZ hat eine unerwartete Antwort gesendet. Die Schnittstelle wurde geändert und der User-Agent dieser Software muss angepasst werden."
            case .unexpectedResponse, .missingData, .tanListDownloadRequired, .tanSetupRequired:
                return "DMRZ hat eine unerwartete Antwort gesendet. Möglicherweise hat sich die Schnittstelle geändert und diese Software muss angepasst werden."
            case .newPasswordRequired:
                return "DMRZ möchte, dass Sie ein neues Passwort vergeben, weil Ihr aktuelles Passwort demnächst abläuft. <a \(externalLinkAttributes)>Bitte loggen Sie sich zuerst bei DRMZ \(externalLinkIcon) ein</a> um ihr neues Passwort zu speichern."
            }
        } else if let error = error as? ValidationError {
            let translations = ["is": "ist", "empty": "leer", "username": "DMRZ Benutzername", "password": "Passwort", "tan": "DMRZ-TAN-Nummer", "less than required minimum of": "kürzer als", "greater than required maximum of": "länger als", "characters": "Zeichen"]
            var message = error.reason
            for (source, target) in translations {
                message = message.replacingOccurrences(of: source, with: target)
            }
            return message
        } else if let error = error {
            return "Leider ist ein unbekannter Fehler beim Anmeldeversuch bei DMRZ aufgetreten. Bitte erneut versuchen. <span class=\"invisible\">\(error.localizedDescription)</span>"
        } else {
            return nil
        }
    }
    
}
