import Vapor

struct RouteScheduleContext {
    let routeSchedule: RouteSchedule
    let date: Date?
    let error: String?
}

extension RouteScheduleContext: Content {}
