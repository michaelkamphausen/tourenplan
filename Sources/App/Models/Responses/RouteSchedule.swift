import Foundation

struct RouteSchedule {
    let tourGroups: [TourGroup]
    let absences: [Absence]?
}

extension RouteSchedule: Codable {}
