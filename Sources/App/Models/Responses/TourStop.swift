struct TourStop {
    enum Kind: Int, Codable {
        case patient = -1
        case `break`
        case physician
        case other
        case pharmacy
        case office
        case setupTime
    }
    
    enum Action: String, Codable {
        case setupMedication
        case transdermalPatch
        case changeBandage
        case shower
        case bath
        case infusion
        case initialConsultation
        case warning
        
        init?(description: String) {
            switch description.lowercased() {
            case let description where description.contains("lk 2"):
                self = .shower
            case let description where description.contains("lk 3"):
                self = .bath
            case let description where description.contains("medikamente richten"):
                self = .setupMedication
            case let description where description.contains("medikamentenpflaster") || description.contains("btm-pflaster"):
                self = .transdermalPatch
            case let description where description.contains("verbandswechsel") || description.contains("wundversorgung") || description.contains("wundverband") || description.contains("wundverbände") || description.contains("dekubitus"):
                self = .changeBandage
            case let description where description.contains("infusion") || description.contains("portversorgung") || description.contains("parenterale ernährung"):
                self = .infusion
            case let description where description.contains("§ 37") || description.contains("§37"):
                self = .initialConsultation
            default:
                return nil
            }
        }
    }
    
    let kind: Kind
    let name: String
    let actions: [Action]
    let address: String?
    let remarks: String?
    let durationMinutes: Int?
}

extension TourStop: Codable {}
