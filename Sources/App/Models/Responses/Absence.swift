import Foundation

struct Absence {
    enum Reason: String, Codable {
        case hospital
        case shortTermCare
        case other
    }
    
    let name: String
    let reason: Reason
    let start: Date
    let end: Date
}

extension Absence : Codable {}
