struct TourList {
    let title: String?
    let tours: [Tour]
}

extension TourList: Codable {}
