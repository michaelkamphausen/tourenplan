struct TourGroup {
    let title: String
    let tourLists: [TourList]
}

extension TourGroup: Codable {}
