struct Tour {
    let start: String
    let nurseName: String
    let vehicle: String
    let isLead: Bool
    let stops: [TourStop]
    let weekday: String
}

extension Tour: Codable {}
