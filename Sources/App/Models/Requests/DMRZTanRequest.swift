import Vapor

struct DMRZTanRequest: Content {
    let tan: String
}

extension DMRZTanRequest: Validatable, Reflectable {
    
    static func validations() throws -> Validations<DMRZTanRequest> {
        var validations = Validations(DMRZTanRequest.self)
        try validations.add(\.tan, .count(6...6))
        return validations
    }
    
}
