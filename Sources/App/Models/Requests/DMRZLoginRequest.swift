import Vapor

struct DMRZLoginRequest: Content {
    let username: String
    let password: String
}

extension DMRZLoginRequest: Validatable, Reflectable {
    
    static func validations() throws -> Validations<DMRZLoginRequest> {
        var validations = Validations(DMRZLoginRequest.self)
        try validations.add(\.username, !.empty)
        try validations.add(\.password, !.empty)
        return validations
    }
    
}
