import Vapor

struct DMRZSessionRequest: Content {
    let dmrzUrl: String
}

extension DMRZSessionRequest: Validatable, Reflectable {
    
    static func validations() throws -> Validations<DMRZSessionRequest> {
        var validations = Validations(DMRZSessionRequest.self)
        try validations.add(\.dmrzUrl, .url)
        return validations
    }
    
}
