import Foundation

extension String {
    
    var abbr: String? {
        if let first = first {
            return String(first) + "."
        } else {
            return nil
        }
    }
    
}

extension String {
    
    func substring(regex: String?) -> String {
        guard let pattern = regex,
            let regex = try? NSRegularExpression(pattern: pattern, options: .dotMatchesLineSeparators),
            case let firstMatch = regex.rangeOfFirstMatch(in: self, range: NSMakeRange(0, self.count)),
            firstMatch.location != NSNotFound,
            let range = Range(firstMatch, in: self) else {
                return self
        }
        
        return String(self[range])
    }
    
}
