import Foundation

extension URL: ExpressibleByStringLiteral {
    
    public init(stringLiteral value: StringLiteralType) {
        self = URL(validString: "\(value)")
    }
    
}

extension URL {
    
    public init(validString value: String) {
        self = URL(string: "\(value)").require(hint: "Invalid URL string literal: \(value)")
    }
    
    func addQueryItems(_ item: [URLQueryItem]) -> URL {
        guard var components = URLComponents(url: self, resolvingAgainstBaseURL: false) else {
            return self
        }
        
        let currentItems = components.queryItems ?? []
        components.queryItems = currentItems + item
        return components.url ?? self
    }
    
}
