import Vapor

extension Validatable {
    
    func validationError() -> Error? {
        do {
            try self.validate()
            return nil
        } catch let error {
            return error
        }
    }
    
}
