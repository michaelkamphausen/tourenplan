import Foundation

extension Date {
    
    func isBetween(_ start: Date, and end: Date) -> Bool {
        return (min(start, end) ... max(start, end)).contains(self)
    }
    
}
