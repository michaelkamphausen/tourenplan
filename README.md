[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE) [![Swift 5.0](https://img.shields.io/badge/swift-5.0-brightgreen.svg)](https://swift.org)

# Tourenplan

Tourenplan-Ansicht für selbstorganisierte Teams in ambulanten Pflegediensten, basierend auf Tourenplan-Daten die in externer Pflege-Software verwaltet werden.

Unterstützt wird aktuell ausschließlich die Pflege-Softare vom [DMRZ](https://www.dmrz.de) (Deutsches Medizinrechenzentrum). Grundsätzlich kann die Tourenplan-Ansicht aber für weitere Pflege-Software erweitert werden. Dies ist Open Source Projekt aus persönlicher Initiative, eine organisatorische Verbindung zur Deutsches Medizinrechenzentrum GmbH besteht nicht.

Das Projekt Tourenplan ist aktuell nur in deutscher Sprache nutzbar.

## Demo

Die Tourenplan-Ansicht kann [hier ausprobiert und produktiv eingesetzt](https://tourenplan.cooperativecare.de) werden. Es sind eigene Zugangsdaten für DMRZ erforderlich um die Anwendung nutzen zu können.

## Funktionen

* Anzeige der Touren mit zuständiger Pflegekraft, Fahrzeug und den Patienten, unterteilt in Früh- und Spätdienst
* Anzeige der durch Pflegeunterbrechung abwesenden Patienten, damit niemand bei der Planung übersehen wird
* Icons um besondere Pflegeleistungen je Einsatz (z.B. Medikamente richten, Verbandwechsel, …) oder den Grund einer Abwesenheit (z.B. Krankenhaus oder Kurzzeitpflege) zu visualisieren
* responsive Benutzeroberfläche für Nutzung mit Desktop-Browser, Tablet oder Smartphone
* für den Druck optimierte Ansicht

So sieht der Tourenplan als fiktives Beispiel aus:

![Screenshot eines Tourenplans mit Abwesenheiten](Documentation/Tourenplan.png)

Die Software aggregiert die Daten aus der Pflege-Software und reicht sie nur zur Visualisierung an den Nutzer durch, eine persistente Zwischenspeicherung von Daten, insbesondere Gesundheitsdaten ist nicht vorgesehen. Deshalb kommt das Projekt ohne Datenbank aus.

Die Software ist backendseitig in den Programmiersprachen [Swift](https://swift.org) mit dem Web-Framework [Vapor](https://vapor.codes) und clientseitig in JavaScript mit den externen Komponenten [Bootstrap](https://getbootstrap.com), [jQuery](https://jquery.com), [FontAwesome](https://fontawesome.com) und [date-input-polyfill](https://github.com/jcgertig/date-input-polyfill) geschrieben.

## Installation

### Lokal / Development

Voraussetzung ist die Installation von Swift 5.0 und Vapor. Hier sind Anleitungen für [macOS](https://docs.vapor.codes/3.0/install/macos/) und [Ubuntu](https://docs.vapor.codes/3.0/install/ubuntu/).

#### macOS

Unter macOS wird durch Ausführen von `vapor xcode` im Projektverzeichnis ein Xcode-Projekt generiert, so dass Build, Run und Debugging des Tourenplan-Webservers über Xcode erfolgen kann. Läuft der Tourenplan-Webserver, ist er typischerweise unter `http://localhost:8080` erreichbar.

Auch ohne Xcode-Projekt kann der der Tourenplan-Webserver gebaut und gestartet werden. Aufgrund der Dependency zur Library libxml2, wird das Projekt folgendermaßen in der Debug-Konfiguration gebaut:

```
swift build -Xcc -I"$(xcrun --show-sdk-path)/usr/include/libxml2" -Xlinker -lxml2 -c debug
```

und wie folgt gestartet:

```
swift run -Xlinker -lxml2 Run --hostname 127.0.0.1
```

Da das Hosting in Production wahrscheinlich auf einem Ubuntu System erfolgen wird, ist es bei der Weiterentwicklung unter macOS sehr empfehlenswert das Projekt so früh wie möglich unter Ubuntu zu testen. Eine einfache Möglichkeit ist die Nutzung von Docker, das hier [installiert](https://www.docker.com/products/docker-desktop) werden kann (Registrierung erforderlich). Weitere Infos siehe [Docker](#docker).

#### Ubuntu

Unter Ubuntu wird das Projekt mit

```
swift build -c debug
```

gebaut und mit 

```
swift run Run serve --env "development" --hostname 127.0.0.1 --port 8080
```

wird der Tourenplan-Webserver mit dem genannten Hostname und Port lokal gestartet.

#### Docker

Docker-Image bauen für das lokale, interaktive bauen und testen des Tourenplan-Webservers:

```
docker build -f Docker/dev.Dockerfile -t tourenplan-dev --build-arg .
```

Starten des Image als interaktiver Container, so dass wir mit der Shell des Containers verbunden sind:

```
docker run --rm --volume "$(pwd):/app" --workdir "/app" -p 8080:80 --interactive --tty --name tourenplan-dev tourenplan-dev
```

Tourenplan-Webserver in der Debug-Konfiguration bauen im Container über seine Shell:

```
swift build -c debug
```

Tourenplan-Webserver starten:

```
swift run Run serve --env "development" --hostname 0.0.0.0 --port 80
```

Mit dieser Konfiguration ist der Tourenplan-Webserver auf dem Host-System unter `http://127.0.0.1:8080` erreichbar.

#### Konfiguration

Für Impressum und Datenschutzerklärung werden Environment-Variablen ausgewertet mit Angaben zum Verantwortlichen samt Adresse (`ADDRESS`), Vertreter (`REPRESENTATIVE`) und Kontaktdaten (`CONTACT`) mit E-Mail-Adresse und Telefonnummer. Die Environment-Variablen können in einer dotENV-Datei unter `Config/secrets/.env` deklariert werden und dürfen HTML enthalten:

```
ADDRESS = "Name <br> Straße <br> PLZ Stadt"
REPRESENTATIVE = "Name"
CONTACT = "Telefon: … <br> E-Mail: …"
```

### Remote / Production

#### Docker

Das Projekt enthält zwar ein Dockerfile, das zum Bauen eines Docker-Images für Production vorgesehen ist:

```
docker build -f Docker/Dockerfile -t tourenplan --build-arg env=production .
```

**Es wird jedoch dringend davon abgeraten dieses Image ohne Erweiterungen produktiv einzusetzen.** Da der Tourenplan-Webserver mit der Vapor Version 3.x nicht über https ansprechbar ist, muss ein Reverse Proxy wie Nginx davor geschaltet werden, der die Erreichbarkeit nach außen übernimmt und lokal an den Tourenplan-Webserver weiterreicht, der von außen nicht direkt zugänglich sein darf. **Die öffentliche Kommunikation mit dem Tourenplan-Webserver darf zum Schutz der sensiblen, personenbezogenen Daten auf keinen Fall unverschlüsselt über http erfolgen.**

Sicherlich lässt sich das [Dockerfile](Docker/Dockerfile) bei Bedarf so erweitern, dass es die Installation und Konfiguration von Nginx, Supervisor und Certbot für Let's Encrypt Zertifikate beinhaltet. Diese Anleitung beschränkt sich allerdings auf ein [manuelles Deployment](#manuelles-deployment).

#### Manuelles Deployment

Vorausgesetzt wird ein Host mit Ubuntu 18.04 mit aktivierter Firewall, Non-Root-User und ssh-Zugang via SSH-Keys statt Passwort. Desweiteren wird die [Installation von Nginx](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04), [Konfiguration mittels Server Blocks](https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-virtual-hosts-on-ubuntu-16-04) und die [Einrichtung von Let's Encrypt Zertifikaten mittels Certbot](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-18-04) vorausgesetzt. Nach dem Deployment sollte der Tourenplan-Webserver außerdem für den automatischen Neustart mit [Supervisor eingerichtet](https://medium.com/@ankitank/deploy-a-basic-vapor-app-with-nginx-and-supervisor-1ef303320726) werden.

Beginnen wir damit, den Tourenplan-Webserver für Production zu bauen. Für macOS wird empfohlen, den [Development-Docker-Container](#docker) dafür zu nutzen. Vorausgesetzt wird also ein Ubuntu 18.04 System mit Swift 5.0. So wird das Projekt in der Release-Konfiguration gebaut:

```
swift build -c release
```

Für das Deployment mit rsync wird die Binary `Run`, sowie die Verzeichnisse `Public`, `Resources` und `Config` vom lokalen Host-System auf den Ubuntu-Server kopiert. 

```
rsync -a --chown=<USER>:<USER> --exclude=.DS_Store --exclude=.gitkeep \
.build/x86_64-unknown-linux/release/Run Public Resources Config \
<USER>@<SERVER-IP>:/home/<username>/tourenplan/
```

Außerdem wird die Swift-Runtime auf dem Ubuntu-Server benötigt. Dafür kann entweder Swift auf dem Ubuntu-Server installiert werden, empfehlenswerter ist allerdings die Swift-Runtime vom lokalen System ebenfalls zum Server zu deployen. So bleibt das Server-System schlanker, das Bauen direkt auf dem Server ist ohnehin nicht empfehlenswert. Zuerst kopieren wir die Swift Runtime Libraries in den Ordner `.build/lib` im lokalen Projektverzeichnis mit folgendem Befehl; beim Bauen mittels Docker-Container wird dieser in der Shell des Containers ausgeführt:

```
mkdir -p .build/lib && cp -R /usr/lib/swift/linux/*.so* .build/lib
```

Dann folgt das eigentliche Deployment der Libraries, das als root-User ausgeführt werden muss:

```
rsync -a --chown=root:root --exclude=.DS_Store --exclude=.gitkeep \
.build/lib/* \
root@<SERVER-IP>:/usr/lib/
```
Die Libraries müssen nur einmalig und bei jeder Änderung der Swift-version deployed werden.

Zum Testen wird der Tourenplan-Webserver remote folgendermaßen gestartet, eine ssh-Verbindung zum Ubuntu-Server vorausgesetzt (`ssh <USER>@<SERVER-IP>`):

```
cd tourenplan
./Run serve --env "production" --hostname <SERVER-IP>
```

Sofern Supervisor bereits eingerichtet ist (jetzt ist ein guter Zeitpunkt), muss dieser außerdem nach jedem Deployment der Binary remote neugestartet werden:

```
sudo supervisorctl restart tourenplan
```



