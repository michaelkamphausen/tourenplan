// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "Tourenplanung",
    dependencies: [
        .package(url: "https://github.com/michaelkamphausen/Fuzi.git", .branch("swift4.2")),
        .package(url: "https://github.com/michaelkamphausen/template-kit.git", .revision("6a0ddae")),
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0")
    ],
    targets: [
        .target(name: "App", dependencies: ["Fuzi", "TemplateKit", "Leaf", "Vapor"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

