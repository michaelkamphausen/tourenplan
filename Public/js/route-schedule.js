(function () {
  var $dayInput = $("[name=day]").eq(0);
  var $nav = $("nav").eq(0);
  var $loadingSpinner = $("#loadingSpinner").eq(0);

  var stepByDays = function ($dayInput, days) {
    var value = $dayInput.val();
    if (!value) {
      return;
    }
    var fromDate = new Date($dayInput.val());
    var toDate = new Date(fromDate.getTime() + 86400000 * days);
    $dayInput.val(toDate.toISOString().slice(0, 10));
    $dayInput.trigger("change");
  };

  $("#button-previous-day").on("click", function () {
    stepByDays($dayInput, -1);
  });
  $("#button-next-day").on("click", function () {
    stepByDays($dayInput, +1);
  });

  var currentRequest;
  var $loadedContent;
  var minute = 60 * 1000;
  var keepAliveDuration = 5 * minute;
  var keepAliveTimer;
  var keepAliveStart;

  var errorHandler = function (xhr, data, status) {
    if (xhr.status == 403) {
      alert("Die DMRZ-Sitzung ist abgelaufen.");
      location.reload();
    }
  };

  var keepAlive = function () {
    if (currentRequest != null) {
      return;
    }

    currentRequest = $.get("/auth/keepalive")
      .always(function () {
        currentRequest = null;
      })
      .done(function () {
        setKeepAlive();
      })
      .fail(errorHandler);
  };

  var setKeepAlive = function () {
    var duration = keepAliveDuration;
    if (keepAliveStart) {
      duration = Math.max(
        minute,
        Math.min(
          keepAliveStart + 2 * keepAliveDuration - new Date().getTime(),
          keepAliveDuration
        )
      );
    }
    keepAliveStart = new Date().getTime();
    keepAliveTimer = setTimeout(keepAlive, duration);
  };

  var clearKeepAlive = function () {
    clearTimeout(keepAliveTimer);
    keepAliveStart = 0;
  };

  $dayInput.on("change", function () {
    var value = $dayInput.val();
    if (!value) {
      return;
    }

    clearKeepAlive();

    if (currentRequest != null) {
      currentRequest.abort();
    }

    $loadingSpinner.removeClass("d-none").addClass("d-flex");

    if ($loadedContent != null) {
      $loadedContent.remove();
      $loadedContent = null;
    }

    currentRequest = $.get("/tourenplanung/" + value)
      .always(function () {
        currentRequest = null;
        $loadingSpinner.removeClass("d-flex").addClass("d-none");
      })
      .done(function (data) {
        $loadedContent = $(data);
        $nav.after($loadedContent);

        $loadedContent.find(".tab-pane").each(function (index, pane) {
          $pane = $(pane);
          $cols = $pane.find(".col");
          var maxListLength = Math.max.apply(
            Math,
            $cols.map(function (index, item) {
              return $(item).find("li").length + ($(item).find("h5").length) * 1.64;
            })
          );
          console.log(">>>", maxListLength);

          if (maxListLength >= 20 || $cols.length > 7) {
            $pane.addClass("print-fontsize-s");
          } else if (maxListLength >= 24) {
            $pane.addClass("print-fontsize-xs");
          } else if (maxListLength <= 16 && $cols.length < 7) {
            $pane.addClass("print-fontsize-l");
            // } else if (maxListLength <= 13) {
            //     $pane.addClass("print-fontsize-xl");
          }

          $pane.find(".duration").each(function () {
            var value = parseInt(this.innerHTML) || 0;
            var hours = Math.ceil(value / 60);
            var minutes = "" + (value % 60);
            if (minutes.length == 1) {
              minutes = "0" + minutes;
            }
            $(this).text(hours + ":" + minutes);
          });
        });

        setKeepAlive();
      })
      .fail(function (xhr, data, status) {
        if (xhr.status >= 500 && xhr.status <= 599) {
          var message =
            "Beim Laden ist ein unerwarteter Fehler aufgetreten. Bitte versuchen Sie es noch einmal.";
          if (xhr.status == 503) {
            message =
              "Der Tourenplan-Webserver ist momentan nicht erreichbar. Bitte probieren Sie es später erneut.";
          }
          $loadedContent = $(
            '<div class="alert alert-light" role="alert">' + message + "</div>"
          );
          $nav.after($loadedContent);
          setKeepAlive();
        } else {
          errorHandler(xhr, data, status);
        }
      });
  });

  $dayInput.trigger("change");
})();
